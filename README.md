<p align="center">
  <a href="https://www.gatsbyjs.com/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  colabor.ar platform: GatsbyJS + MUI
</h1>

## 🚀 Quick start

1.  **Dependencias**

  - NodeJS: v17.0.1
  - Gatsby CLI: 4.24.0
  - Gatsby: 4.11.2
     ```shell
    Acá puede ir un instructivo para instalar las dependencias
    ```

2.  **Ejecución**

    Navigate into your new site’s directory and start it up.

    ```shell
    gatsby develop
    ```

    Your site is now running at http://localhost:8000!